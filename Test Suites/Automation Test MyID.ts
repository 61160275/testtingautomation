<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Automation Test MyID</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>905cd457-5d7b-438d-9898-5c38be2ff499</testSuiteGuid>
   <testCaseLink>
      <guid>82d2d7c6-e06d-451d-8360-467496e81c01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eec91cfb-2ff3-4fb3-8363-5c7376cd0a97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Fail with worng username</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>004e0a4d-98b9-404e-a19a-17962e19c88d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Fail with worng password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8196df0f-ebf6-46e2-b09c-64c5add11d96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60bd01e1-4e9b-466c-831c-87ed305c23b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Chang Password Fail with Less than 8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a9a6c1c-2f47-4132-a1b7-d7e532062824</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Fail with No Character</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa469c2d-da38-4819-8246-da6ac1541c1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Fail with No Number Letters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a17cf8f8-aac0-4964-aacf-9293cc6921b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Fail with No Symbol Letters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa61552c-93dd-4df3-8bbc-76c594b7b42b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
